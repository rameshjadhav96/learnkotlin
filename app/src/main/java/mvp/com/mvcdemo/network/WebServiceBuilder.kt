package mvp.com.mvcdemo.network


import mvp.com.mvcdemo.model.BlockCardResponse
import mvp.com.mvcdemo.model.CardDetailsResponse
import mvp.com.mvcdemo.model.CardTransactionResponse
import mvp.com.mvcdemo.model.CreatePayCardModel
import mvp.com.mvcdemo.model.EmailMobileAvailableResponse
import mvp.com.mvcdemo.model.LoginDetailsResponse
import mvp.com.mvcdemo.model.MECycleResponse
import mvp.com.mvcdemo.model.MoneyEarnedModel
import mvp.com.mvcdemo.model.OTPGenerationModel
import mvp.com.mvcdemo.model.SessionResponse
import mvp.com.mvcdemo.model.StateModel
import mvp.com.mvcdemo.model.TransactionListResponse
import mvp.com.mvcdemo.model.USPSAddressModel
import mvp.com.mvcdemo.model.UpdateAlertPreferenceResponse
import mvp.com.mvcdemo.model.VerifyPasswordResponse
import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

class WebServiceBuilder {
    internal object Urls {
        const val GET_LANDING_SCREEN_DATA_URL = "/users/cycle"
        const val GET_CARD_DETAILS_URL = "/users/payrollcard"
        const val GET_TRANSACTION_LIST_DATA_URL = "/users/transactions"
        const val LOGOUT_FROM_APPLICATION_URL = "/session"
        const val GET_LIST_OF_STATES_URL = "/paycard/states"
        const val GET_USER_DETAILS_URL = "/users/{employer_id}/{employee_id}"
        const val CHECK_EMAIL_AND_MOBILE_AVAILABLE_URL = "/mobile_email/is_available"
        const val CONFIRM_MONEY_EARNED_URL = "/users/money_earned"
        const val GENERATE_OTP_URL = "/otps"
        const val VALIDATE_OTP_URL = "/validate_otp"
        const val SIGN_UP_URL = "/users/signup"
        const val CREATE_PAYCARD_URL = "/users/payrollcard"
        const val VERIFY_USER_URL = "/users/verify"
        const val VERIFY_ADDRESS_URL = "/address_validation"
        const val LOGIN_URL = "/session"
        const val GENERATE_UPDATE_OTP_URL = "/otps/update"
        const val UPDATE_MOBILE_EMAIL_AND_ADDRESS_URL = "/user"
        const val FORGOT_PASSWORD_GENERATE_OTP_URL = "/otps"
        const val FORGOT_PASSWORD_VALIDATE_OTP_URL = "/validate_otp"
        const val VERIFY_PASSWORD_URL = "/verify_password"
        const val UPDATE_PIN_URL = "/users/payrollcard/pin"
        const val FREEZE_CARD_URL = "/users/payrollcard/block"
        const val UNFREEZE_CARD_URL = "/users/payrollcard/unblock"
        const val VALIDATE_CARD_HOLDER_URL = "/cardholder/validate"
        const val ACTIVATE_CARD_URL = "/users/payrollcard/activate"
        const val UPDATE_ALERT_PREFERENCE_URL = "/users/payrollcard/alert_preference"
        const val CARD_ACTIVITY_URL = "/users/cardactivity"
    }

    internal object HeaderParam {
        const val USER_SESSION_ID = "USERSESSIONID"
        const val COMPANY_ID = "COMPANYID"
    }

    internal object QueryParams {
        const val GET_PAYCARD_ACCOUNTS = "get_paycard_accounts"
        const val EMPLOYER_ID = "employer_id"
        const val EMPLOYEE_ID = "employee_id"
        const val EMAIL = "email"
        const val MOBILE_NUMBER = "mobile_number"
        const val TRANSACTION_TYPE = "transaction_type"
        const val TRANSACTION_AMOUNT = "transaction_amount"
        const val FEES = "fees"
        const val ACCOUNT_NUMBER = "account_number"
        const val CARD_LAST_FOUR = "card_last_four"
        const val DOB = "dob"
        const val OTP = "otp"
        const val MOBILE_PASSWORD = "mobile_password"
        const val FIRST_NAME = "first_name"
        const val ADDRESS1 = "address1"
        const val ADDRESS2 = "address2"
        const val STATE = "state"
        const val CITY = "city"
        const val ZIP = "zip"
        const val SSN = "ssn"
        const val EMAIL_ID = "email_id"
        const val AUTH_DATA = "auth_data"
        const val UPDATE_TYPE = "update_type"
        const val REQUEST_TYPE = "request_type"
        const val VERIFICATION_TOKEN = "verification_token"
        const val CARD_PIN = "card_pin"
        const val CARD_ACCOUNT_ID = "card_account_id"
        const val TWO_FACTOR_AUTH = "twoFactorAuth"
        const val USER_NAME = "username"
        const val BALANCE_ALERTS = "balance_alerts"
        const val TRANSACTION_ALERTS = "transaction_alerts"
        const val HIGH_RISK_TRANSACTION_ALERTS = "high_risk_transaction_alerts"
        const val IP_ADDRESS = "ipAddress"
        const val TXN_ID_NUMBER = "txn_id_number"
        const val QUESTION1_TYPE = "question1_type"
        const val QUESTION1_ANSWER = "question1_answer"
        const val QUESTION2_TYPE = "question2_type"
        const val QUESTION2_ANSWER = "question2_answer"
        const val QUESTION3_TYPE = "question3_type"
        const val QUESTION3_ANSWER = "question3_answer"
        const val RRN = "rrn"
        // Needs to do same name
        const val SSN_LAST_FOUR = "ssn_last_four"
        const val ssnLastFour = "ssnLastFour"
        const val ssnLastfour = "ssnlastfour"
        const val LAST_NAME = "last_name"
        const val lastName = "lastname"
        const val DEVICE_ID = "deviceID"
        const val REPOSITION_DATE_COMPLEMENT = "reposition_date_complement"
        const val REPOSITION_DATE = "reposition_date"
        const val REPOSITION_TIME = "reposition_time"
        const val REPOSITION_TIMESTAMP = "reposition_timestamp"
    }

    interface GetAPIs {
        @GET(Urls.GET_LANDING_SCREEN_DATA_URL)
        fun getMETabData(@Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                         @Header(HeaderParam.COMPANY_ID) companyId: String, @Query(QueryParams.GET_PAYCARD_ACCOUNTS) getPayCardAccounts: String): Call<MECycleResponse>

        @GET(Urls.GET_CARD_DETAILS_URL)
        fun getCardDetails(@Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                           @Header(HeaderParam.COMPANY_ID) companyId: String, @Query(QueryParams.GET_PAYCARD_ACCOUNTS) getPayCardAccounts: String): Call<CardDetailsResponse>

        @GET(Urls.GET_TRANSACTION_LIST_DATA_URL)
        fun getTransactionListData(@Header(HeaderParam.USER_SESSION_ID) userSessionId: String?,
                                   @Header(HeaderParam.COMPANY_ID) companyId: String?): Call<TransactionListResponse>

        @DELETE(Urls.LOGOUT_FROM_APPLICATION_URL)
        fun logoutFromApplication(@Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                                  @Header(HeaderParam.COMPANY_ID) companyId: String): Call<LoginDetailsResponse>

        @GET(Urls.GET_LIST_OF_STATES_URL)
        fun getListOfStates(@Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<StateModel>

        @GET(Urls.GET_USER_DETAILS_URL)
        fun getUserDetails(@Path(QueryParams.EMPLOYER_ID) employerId: String, @Path(QueryParams.EMPLOYEE_ID) employeeId: String): Call<LoginDetailsResponse>

        @GET(Urls.CHECK_EMAIL_AND_MOBILE_AVAILABLE_URL)
        fun checkEmailAndMobileAvailable(@Query(QueryParams.EMPLOYEE_ID) employeeId: String,
                                         @Query(QueryParams.EMAIL) email: String, @Query(QueryParams.MOBILE_NUMBER) mobileNumber: String,
                                         @Header(HeaderParam.COMPANY_ID) companyId: String): Call<EmailMobileAvailableResponse>

        @GET(Urls.CARD_ACTIVITY_URL)
        fun getCardActivityList(@Query(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String,
                                @Query(QueryParams.CARD_LAST_FOUR) cardLastFour: String,
                                @Query(QueryParams.REPOSITION_DATE_COMPLEMENT) repositionDateComplement: String,
                                @Query(QueryParams.REPOSITION_DATE) repositionDate: String, @Query(QueryParams.REPOSITION_TIME) repositionTime: String,
                                @Query(QueryParams.REPOSITION_TIMESTAMP) repositionTimestamp: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                                @Header(HeaderParam.COMPANY_ID) companyId: String): Call<CardTransactionResponse>
    }

    interface PostAPIs {
        @FormUrlEncoded
        @POST(Urls.CONFIRM_MONEY_EARNED_URL)
        fun confirmMoneyEarned(@Field(QueryParams.TRANSACTION_TYPE) transactionType: String,
                               @Field(QueryParams.TRANSACTION_AMOUNT) transactionAmount: String, @Field(QueryParams.FEES) fees: String,
                               @Field(QueryParams.ACCOUNT_NUMBER) accountNumber: String, @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String,
                               @Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<MoneyEarnedModel>

        @FormUrlEncoded
        @POST(Urls.GENERATE_OTP_URL)
        fun generateOTP(@Field(QueryParams.EMPLOYEE_ID) employeeId: String, @Field(QueryParams.MOBILE_NUMBER) mobileNumber: String,
                        @Field(QueryParams.DOB) dob: String, @Field(QueryParams.ssnLastFour) ssnLastFour: String, @Field(QueryParams.lastName) lastName: String,
                        @Header(HeaderParam.COMPANY_ID) companyId: String): Call<OTPGenerationModel>

        @FormUrlEncoded
        @POST(Urls.VALIDATE_OTP_URL)
        fun validateOTP(@Field(QueryParams.OTP) otp: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                        @Header(HeaderParam.COMPANY_ID) companyId: String): Call<OTPGenerationModel>

        @FormUrlEncoded
        @POST(Urls.SIGN_UP_URL)
        fun signUp(@Field(QueryParams.MOBILE_NUMBER) mobileNumber: String, @Field(QueryParams.OTP) otp: String,
                   @Field(QueryParams.MOBILE_PASSWORD) mobilePassword: String, @Field(QueryParams.EMPLOYEE_ID) employeeId: String,
                   @Field(QueryParams.EMAIL) email: String, @Field(QueryParams.DOB) dob: String, @Field(QueryParams.ssnLastfour) ssnLastFour: String,
                   @Field(QueryParams.lastName) lastName: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<SessionResponse>

        @FormUrlEncoded
        @POST(Urls.CREATE_PAYCARD_URL)
        fun createPayCard(@Field(QueryParams.MOBILE_NUMBER) mobileNumber: String, @Field(QueryParams.FIRST_NAME) firstName: String,
                          @Field(QueryParams.LAST_NAME) lastName: String, @Field(QueryParams.ADDRESS1) address1: String,
                          @Field(QueryParams.ADDRESS2) address2: String, @Field(QueryParams.STATE) state: String, @Field(QueryParams.CITY) city: String,
                          @Field(QueryParams.ZIP) zip: String, @Field(QueryParams.DOB) dob: String, @Field(QueryParams.SSN) ssn: String,
                          @Field(QueryParams.EMAIL_ID) emailId: String, @Field(QueryParams.IP_ADDRESS) ipAddress: String,
                          @Field(QueryParams.DEVICE_ID) deviceID: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                          @Header(HeaderParam.COMPANY_ID) companyId: String): Call<CreatePayCardModel>

        @FormUrlEncoded
        @POST(Urls.CREATE_PAYCARD_URL)
        fun createPayCardWithIdeology(@Field(QueryParams.MOBILE_NUMBER) mobileNumber: String,
                                      @Field(QueryParams.FIRST_NAME) firstName: String, @Field(QueryParams.LAST_NAME) lastName: String,
                                      @Field(QueryParams.ADDRESS1) address1: String, @Field(QueryParams.ADDRESS2) address2: String, @Field(QueryParams.STATE) state: String,
                                      @Field(QueryParams.CITY) city: String, @Field(QueryParams.ZIP) zip: String, @Field(QueryParams.DOB) dob: String,
                                      @Field(QueryParams.SSN) ssn: String, @Field(QueryParams.EMAIL_ID) emailId: String, @Field(QueryParams.IP_ADDRESS) ipAddress: String,
                                      @Field(QueryParams.TXN_ID_NUMBER) txnIdNumber: String, @Field(QueryParams.QUESTION1_TYPE) question1Type: String,
                                      @Field(QueryParams.QUESTION1_ANSWER) question1Answer: String, @Field(QueryParams.QUESTION2_TYPE) question2Type: String,
                                      @Field(QueryParams.QUESTION2_ANSWER) question2Answer: String, @Field(QueryParams.QUESTION3_TYPE) question3Type: String,
                                      @Field(QueryParams.QUESTION3_ANSWER) question3Answer: String, @Field(QueryParams.RRN) rrn: String,
                                      @Field(QueryParams.DEVICE_ID) deviceID: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                                      @Header(HeaderParam.COMPANY_ID) companyId: String): Call<CreatePayCardModel>

        @FormUrlEncoded
        @POST(Urls.VERIFY_USER_URL)
        fun verifyUser(@Field(QueryParams.EMPLOYEE_ID) employeeId: String, @Field(QueryParams.DOB) dob: String,
                       @Field(QueryParams.SSN) ssn: String, @Field(QueryParams.lastName) lastName: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<SessionResponse>

        @FormUrlEncoded
        @POST(Urls.VERIFY_ADDRESS_URL)
        fun verifyAddress(@Field(QueryParams.ADDRESS1) address1: String, @Field(QueryParams.ADDRESS2) address2: String,
                          @Field(QueryParams.CITY) city: String, @Field(QueryParams.STATE) state: String, @Field(QueryParams.ZIP) zip: String,
                          @Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<USPSAddressModel>

        @POST(Urls.LOGIN_URL)
        @FormUrlEncoded
        fun login(@Field(QueryParams.USER_NAME) userName: String, @Field(QueryParams.AUTH_DATA) authData: String,
                  @Field(QueryParams.TWO_FACTOR_AUTH) twoFactorAuth: String): Call<SessionResponse>

        @FormUrlEncoded
        @POST(Urls.GENERATE_UPDATE_OTP_URL)
        fun generateUpdateOTP(@Field(QueryParams.UPDATE_TYPE) updateType: String,
                              @Field(QueryParams.MOBILE_NUMBER) mobileNumber: String, @Field(QueryParams.EMAIL_ID) emailId: String,
                              @Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<OTPGenerationModel>

        @FormUrlEncoded
        @PUT(Urls.UPDATE_MOBILE_EMAIL_AND_ADDRESS_URL)
        fun updateMobileEmailAndAddress(@Field(QueryParams.UPDATE_TYPE) updateType: String,
                                        @Field(QueryParams.MOBILE_NUMBER) mobileNumber: String, @Field(QueryParams.EMAIL_ID) emailId: String,
                                        @Field(QueryParams.ADDRESS1) address1: String, @Field(QueryParams.ADDRESS2) address2: String, @Field(QueryParams.CITY) city: String,
                                        @Field(QueryParams.ZIP) zip: String, @Field(QueryParams.STATE) state: String,
                                        @Field(QueryParams.VERIFICATION_TOKEN) verificationToken: String, @Field(QueryParams.OTP) otp: String,
                                        @Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<OTPGenerationModel>

        @FormUrlEncoded
        @POST(Urls.FORGOT_PASSWORD_GENERATE_OTP_URL)
        fun forgotPasswordGenerateOTP(@Field(QueryParams.REQUEST_TYPE) requestType: String,
                                      @Field(QueryParams.EMPLOYEE_ID) employeeId: String, @Field(QueryParams.MOBILE_NUMBER) mobileNumber: String,
                                      @Field(QueryParams.EMAIL_ID) emailId: String): Call<OTPGenerationModel>

        @FormUrlEncoded
        @POST(Urls.FORGOT_PASSWORD_VALIDATE_OTP_URL)
        fun forgotPasswordValidateOTP(@Field(QueryParams.REQUEST_TYPE) requestType: String,
                                      @Field(QueryParams.EMPLOYEE_ID) employeeId: String, @Field(QueryParams.MOBILE_NUMBER) mobileNumber: String,
                                      @Field(QueryParams.EMAIL_ID) emailId: String, @Field(QueryParams.OTP) otp: String): Call<OTPGenerationModel>

        @FormUrlEncoded
        @POST(Urls.VERIFY_PASSWORD_URL)
        fun verifyPassword(@Field(QueryParams.AUTH_DATA) authData: String,
                           @Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<VerifyPasswordResponse>

        @FormUrlEncoded
        @PUT(Urls.UPDATE_PIN_URL)
        fun updatePin(@Field(QueryParams.VERIFICATION_TOKEN) verificationToken: String,
                      @Field(QueryParams.CARD_PIN) cardPin: String, @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String,
                      @Field(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                      @Header(HeaderParam.COMPANY_ID) companyId: String): Call<VerifyPasswordResponse>

        @FormUrlEncoded
        @PUT(Urls.FREEZE_CARD_URL)
        fun freezeCard(@Field(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String,
                       @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                       @Header(HeaderParam.COMPANY_ID) companyId: String): Call<BlockCardResponse>

        @FormUrlEncoded
        @PUT(Urls.UNFREEZE_CARD_URL)
        fun unFreezeCard(@Field(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String,
                         @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                         @Header(HeaderParam.COMPANY_ID) companyId: String): Call<BlockCardResponse>

        @FormUrlEncoded
        @POST(Urls.VALIDATE_CARD_HOLDER_URL)
        fun validateCardHolder(@Field(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String,
                               @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String, @Field(QueryParams.SSN_LAST_FOUR) ssnLastFour: String,
                               @Header(HeaderParam.USER_SESSION_ID) userSessionID: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<VerifyPasswordResponse>

        @FormUrlEncoded
        @PUT(Urls.ACTIVATE_CARD_URL)
        fun activateCard(@Field(QueryParams.VERIFICATION_TOKEN) verificationToken: String,
                         @Field(QueryParams.CARD_PIN) cardPin: String, @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String,
                         @Field(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String, @Header(HeaderParam.USER_SESSION_ID) userSessionId: String,
                         @Header(HeaderParam.COMPANY_ID) companyId: String): Call<VerifyPasswordResponse>

        @FormUrlEncoded
        @PUT(Urls.UPDATE_ALERT_PREFERENCE_URL)
        fun updateAlertPreference(@Field(QueryParams.CARD_ACCOUNT_ID) cardAccountId: String,
                                  @Field(QueryParams.CARD_LAST_FOUR) cardLastFour: String, @Field(QueryParams.BALANCE_ALERTS) balanceAlerts: Boolean,
                                  @Field(QueryParams.TRANSACTION_ALERTS) transactionAlerts: Boolean,
                                  @Field(QueryParams.HIGH_RISK_TRANSACTION_ALERTS) highRiskTransactionAlerts: Boolean,
                                  @Header(HeaderParam.USER_SESSION_ID) userSessionId: String, @Header(HeaderParam.COMPANY_ID) companyId: String): Call<UpdateAlertPreferenceResponse>
    }
}
