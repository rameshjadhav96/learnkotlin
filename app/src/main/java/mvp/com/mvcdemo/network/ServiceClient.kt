package mvp.com.mvcdemo.network

import android.util.Base64
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ServiceClient {
    private constructor() {

    }

    fun <T> createService(serviceClass: Class<T>): T {
        httpClient.connectTimeout(CONNECTION_TIMEOUT_FOR_ALL.toLong(), TimeUnit.SECONDS).readTimeout(READ_TIMEOUT_FOR_ALL.toLong(), TimeUnit.SECONDS)
        val retrofit = builder.client(
                httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build()).build()
        return retrofit.create(serviceClass)
    }

    fun <T> createService(serviceClass: Class<T>, readTimeout: Int): T {
        httpClient.connectTimeout(CONNECTION_TIMEOUT_FOR_ALL.toLong(), TimeUnit.SECONDS).readTimeout(readTimeout.toLong(), TimeUnit.SECONDS)
        val retrofit = builder.client(
                httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build()).build()
        return retrofit.create(serviceClass)
    }

    companion object {
        private val READ_TIMEOUT_FOR_ALL = 30 // 30 seconds
        private val CONNECTION_TIMEOUT_FOR_ALL = 15 // 15 seconds
        private val httpClient = OkHttpClient.Builder()
        private val builder = Retrofit.Builder().baseUrl("https://cert.ganart.net:7021").addConverterFactory(
                GsonConverterFactory.create())

        private fun encodeCredentialsForBasicAuthorization(): String {
            val userAndPassword = "GANART_CERT:W3SYuNiMqWLqXdKZx7lqbDQ6CQDzlhEceL7HkQC4kU1oQXFRfFm49PCP5UGlz9na"
            return "Basic " + Base64.encodeToString(userAndPassword.toByteArray(), Base64.NO_WRAP)
        }
        private var instance1: ServiceClient? = null

        fun getInstance(): ServiceClient {
            if (instance1 == null) {
                instance1 = ServiceClient()
                httpClient.addInterceptor { chain ->
                    val request = chain.request().newBuilder().header("Authorization", encodeCredentialsForBasicAuthorization()).build()
                    chain.proceed(request)
                }
            }
            return instance1!!
        }
    }
}
