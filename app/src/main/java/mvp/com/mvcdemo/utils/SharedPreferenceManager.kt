package mvp.com.mvcdemo.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

import com.google.gson.Gson
import mvp.com.mvcdemo.model.CardDetailsResponse
import mvp.com.mvcdemo.model.SessionResponse

object SharedPreferenceManager {
    private val IS_FINGERPRINT_ENABLED = "IS_FINGERPRINT_ENABLED"
    private val HAS_USER_SIGNED_UP = "HAS_USER_SIGNED_UP"

    private val CUSTOMER_SUPPORT_NUMBER = "CUSTOMER_SUPPORT_NUMBER"
    private val CUSTOMER_SUPPORT_EMAIL = "CUSTOMER_SUPPORT_EMAIL"

    private val SESSION_ID = "SESSION_ID"
    private val SITE = "SITE"
    private val SITE_NAME = "SITE_NAME"
    private val MOBILE_NUMBER = "MOBILE_NUMBER"
    private val EMPLOYEE_ID = "EMPLOYEE_ID"
    private val EMPLOYEE_FIRST_NAME = "EMPLOYEE_FIRST_NAME"
    private val EMPLOYEE_LAST_NAME = "EMPLOYEE_LAST_NAME"
    private val EMPLOYEE_DOB = "EMPLOYEE_DOB"
    private val EMPLOYEE_ADDRESS1 = "EMPLOYEE_ADDRESS1"
    private val EMPLOYEE_ADDRESS2 = "EMPLOYEE_ADDRESS2"
    private val EMPLOYEE_CITY = "EMPLOYEE_CITY"
    private val EMPLOYEE_STATE = "EMPLOYEE_STATE"
    private val EMPLOYEE_ZIP = "EMPLOYEE_ZIP"
    private val EMPLOYEE_EMAIL = "EMPLOYEE_EMAIL"
    private val EMPLOYEE_EMPLOYMENT_STATE = "EMPLOYEE_EMPLOYMENT_STATE"
    private val PAY_CARD_CUSTOMER_NUMBER = "PAY_CARD_CUSTOMER_NUMBER"

    private val EMPLOYEE_CYCLE_DETAILS = "EMPLOYEE_CYCLE_DETAILS"
    private val MONEY_EARN_AVAILABLE = "MONEY_EARN_AVAILABLE"
    private val LATEST_TNC_VERSION = "LATEST_TNC_VERSION"
    private val TNC_BASE_URL = "TNC_BASE_URL"
    private var preferences: SharedPreferences? = null

    private fun getPreference(context: Context): SharedPreferences {
        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context)
        }
        return preferences!!
    }

    private fun saveDataToPreferences(mContext: Context, key: String, value: String) {
        val editor = getPreference(mContext).edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun getDataFromPreferences(mContext: Context, key: String, defValue: String): String? {
        return getPreference(mContext).getString(key, defValue)
    }

    private fun saveBooleanDataToPreferences(mContext: Context, key: String, value: Boolean) {
        val editor = getPreference(mContext).edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    private fun getBooleanDataFromPreferences(mContext: Context, key: String, defValue: Boolean): Boolean {
        return getPreference(mContext).getBoolean(key, defValue)
    }

    // User Password
    fun getEncryptedUserPassword(context: Context): String? {
        return getDataFromPreferences(context, AppConstants.AUTH_PASSWORD_KEY, "")
    }

    fun setEncryptedUserPassword(context: Context, encryptedPassword: String) {
        saveDataToPreferences(context, AppConstants.AUTH_PASSWORD_KEY, encryptedPassword)
    }

    // User Fingerprint Settings
    fun isUserFingerprintEnabled(context: Context): Boolean {
        return getBooleanDataFromPreferences(context, IS_FINGERPRINT_ENABLED, false)
    }

    fun enableUserFingerprint(context: Context) {
        saveBooleanDataToPreferences(context, IS_FINGERPRINT_ENABLED, true)
    }

    fun disableUserFingerprint(context: Context) {
        saveBooleanDataToPreferences(context, IS_FINGERPRINT_ENABLED, false)
    }

    // User Unlock PIN Settings
    fun isUserUnlockPINAvailable(context: Context): Boolean {
        return getDataFromPreferences(context, AppConstants.AUTH_UNLOCK_PIN_KEY, "")!!.length > 0
    }

    fun getEncryptedUserUnlockPIN(context: Context): String? {
        return getDataFromPreferences(context, AppConstants.AUTH_UNLOCK_PIN_KEY, "")
    }

    fun setEncryptedUserUnlockPIN(context: Context, encryptedUnlockPIN: String) {
        saveDataToPreferences(context, AppConstants.AUTH_UNLOCK_PIN_KEY, encryptedUnlockPIN)
    }

    // ****************** Employee Preferences ******************//

    fun hasUserSignedUp(context: Context): Boolean {
        return getBooleanDataFromPreferences(context, HAS_USER_SIGNED_UP, false)
    }

    fun setUserSignedUp(context: Context, userSignedUp: Boolean) {
        saveBooleanDataToPreferences(context, HAS_USER_SIGNED_UP, userSignedUp)
    }

    // Employee Cycle Details
    fun setEmployeeCycleDetails(context: Context, CardDetailsResponse: CardDetailsResponse) {
        val gson = Gson()
        val payCards = gson.toJson(CardDetailsResponse)
        saveDataToPreferences(context, EMPLOYEE_CYCLE_DETAILS, payCards)
    }

    fun setSessionResponse(context: Context, sessionResponse: SessionResponse) {
        setSessionID(context, sessionResponse.session_id!!)
        setUserCompany(context, sessionResponse.site!!)
        setUserCompanyName(context, sessionResponse.siteName!!)
        setEmployeeID(context, sessionResponse.employeeID!!)
        setEmployeeFirstName(context, sessionResponse.firstName!!)
        setEmployeeLastName(context, sessionResponse.lastName!!)
        setEmployeeDOB(context, sessionResponse.dob!!)
        setEmployeeEmail(context, sessionResponse.emailContact!!)
        setEmployeeMobile(context, sessionResponse.mobileNumber!!)
        setEmploymentState(context, sessionResponse.employeeState!!)
        setEmployeeAddress1(context, sessionResponse.address1!!)
        setEmployeeAddress2(context, sessionResponse.address2!!)
        setEmployeeCity(context, sessionResponse.city!!)
        setEmployeeState(context, sessionResponse.state!!)
        setEmployeeZip(context, sessionResponse.zip!!)
        setMoneyEarnedAvailable(context, sessionResponse.moneyEarnedAvailable)
        setCustomerSupportNumber(context, sessionResponse.customerSupportNumber!!)
        setCustomerSupportEmail(context, sessionResponse.customerSupportEmail!!)
        // I was forgot to call below method solved earlier.
        setPayCardCustomerNumber(context, sessionResponse.payCardCustomerNumber!!)
        setSessionID(context, sessionResponse.session_id!!)
    }

    fun getSessionID(context: Context): String? {
        return getDataFromPreferences(context, SESSION_ID, "")
    }

    private fun setSessionID(context: Context, sessionID: String) {
        saveDataToPreferences(context, SESSION_ID, sessionID)
    }

    // Employee Site/Company
    fun getUserCompany(context: Context): String? {
        return getDataFromPreferences(context, SITE, "")
    }

    private fun setUserCompany(context: Context, company: String) {
        saveDataToPreferences(context, SITE, company)
    }

    fun getUserCompanyName(context: Context): String ?{
        return getDataFromPreferences(context, SITE_NAME, "")
    }

    private fun setUserCompanyName(context: Context, company: String) {
        saveDataToPreferences(context, SITE_NAME, company)
    }

    // Employee ID
    fun getEmployeeID(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_ID, "")
    }

    // Employee Name
    fun getEmployeeFirstName(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_FIRST_NAME, "")
    }

    private fun setEmployeeFirstName(context: Context, firstName: String) {
        saveDataToPreferences(context, EMPLOYEE_FIRST_NAME, firstName)
    }

    fun getEmployeeLastName(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_LAST_NAME, "")
    }

    private fun setEmployeeLastName(context: Context, lastName: String) {
        saveDataToPreferences(context, EMPLOYEE_LAST_NAME, lastName)
    }

    private fun setEmployeeID(context: Context, employeeID: String) {
        saveDataToPreferences(context, EMPLOYEE_ID, employeeID)
    }

    // Employee DOB
    fun getEmployeeDOB(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_DOB, "")
    }

    private fun setEmployeeDOB(context: Context, dob: String) {
        saveDataToPreferences(context, EMPLOYEE_DOB, dob)
    }

    fun getEmployeeEmail(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_EMAIL, "")
    }

    fun setEmployeeEmail(context: Context, email: String) {
        saveDataToPreferences(context, EMPLOYEE_EMAIL, email)
    }

    // Employee Mobile Number
    fun getEmployeeMobile(context: Context): String? {
        return getDataFromPreferences(context, MOBILE_NUMBER, "")
    }

    fun setEmployeeMobile(context: Context, mobile: String) {
        saveDataToPreferences(context, MOBILE_NUMBER, mobile)
    }

    // Employee Employment State
    fun getEmploymentState(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_EMPLOYMENT_STATE, "")
    }

    private fun setEmploymentState(context: Context, email: String) {
        saveDataToPreferences(context, EMPLOYEE_EMPLOYMENT_STATE, email)
    }

    // Employee Address
    fun getEmployeeAddress1(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_ADDRESS1, "")
    }

    fun setEmployeeAddress1(context: Context, address1: String) {
        saveDataToPreferences(context, EMPLOYEE_ADDRESS1, address1)
    }

    fun getEmployeeAddress2(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_ADDRESS2, "")
    }

    fun setEmployeeAddress2(context: Context, address2: String) {
        saveDataToPreferences(context, EMPLOYEE_ADDRESS2, address2)
    }

    fun getEmployeeCity(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_CITY, "")
    }

    fun setEmployeeCity(context: Context, city: String) {
        saveDataToPreferences(context, EMPLOYEE_CITY, city)
    }

    fun getEmployeeState(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_STATE, "")
    }

    fun setEmployeeState(context: Context, state: String) {
        saveDataToPreferences(context, EMPLOYEE_STATE, state)
    }

    fun getEmployeeZip(context: Context): String? {
        return getDataFromPreferences(context, EMPLOYEE_ZIP, "")
    }

    fun setEmployeeZip(context: Context, zip: String) {
        saveDataToPreferences(context, EMPLOYEE_ZIP, zip)
    }

    private fun setMoneyEarnedAvailable(context: Context, moneyEarnedAvailable: Boolean) {
        saveBooleanDataToPreferences(context, MONEY_EARN_AVAILABLE, moneyEarnedAvailable)
    }

    fun getMoneyEarnedAvailable(context: Context): Boolean {
        return getBooleanDataFromPreferences(context, MONEY_EARN_AVAILABLE, false)
    }

    fun getCustomerSupportNumber(context: Context): String? {
        return getDataFromPreferences(context, CUSTOMER_SUPPORT_NUMBER, "")
    }

    fun setCustomerSupportNumber(context: Context, supportNumber: String) {
        saveDataToPreferences(context, CUSTOMER_SUPPORT_NUMBER, supportNumber)
    }

    fun getCustomerSupportEmail(context: Context): String? {
        return getDataFromPreferences(context, CUSTOMER_SUPPORT_EMAIL, "")
    }

    fun setCustomerSupportEmail(context: Context, supportEmail: String) {
        saveDataToPreferences(context, CUSTOMER_SUPPORT_EMAIL, supportEmail)
    }

    fun getPayCardCustomerNumber(context: Context): String? {
        return getDataFromPreferences(context, PAY_CARD_CUSTOMER_NUMBER, "")
    }

    private fun setPayCardCustomerNumber(context: Context, payCardCustomerNumber: String) {
        saveDataToPreferences(context, PAY_CARD_CUSTOMER_NUMBER, payCardCustomerNumber)
    }

    // ************** Manage Preferences ************** //


    fun deleteAllFromPreference(context: Context) {
        val hasUserSignedUp = hasUserSignedUp(context)
        getPreference(context).edit().clear().apply()
        setUserSignedUp(context, hasUserSignedUp)
    }

}
