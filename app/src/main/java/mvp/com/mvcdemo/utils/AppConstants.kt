package mvp.com.mvcdemo.utils


object AppConstants {
    val BANK_SUPPORT_NUMBER = "833-459-3443"
    val PRIVACY_POLICY = "policies/privacy-policy.html"
    val ELECTRONIC_COMMUNICATION_AGREEMENT = "policies/electronic-communication-agreement.html"
    val CARD_HOLDER_AGREEMENT = "policies/cardholder-agreement.html"
    val FEE_SCHEDULE = "policies/schedule-of-fees.html"
    val TERMS_OF_SERVICE = "policies/terms-of-service.html"
    val MONEY_PASS_WEB_LINK = "https://www.moneypass.com"
    val MONEY_PASS_APP_LINK = "https://play.google.com/store/apps/details?id=com.elan.vendor.moneypass&hl=en"
    val USPS_SERVICE_DISABLED = "Service is disabled."
    val USPS_ADDRESS_NOT_FOUND = "Address Not Found.  "
    val USPS_INVALID_ZIP = "Invalid Zip Code.  "
    val USPS_INVALID_CITY = "Invalid City.  "
    val ACCOUNT_LOCKED = "Account temporarily locked"
    val USER_IS_BLOCKED = "User is blocked."
    val INVALID_TOKEN = "Invalid or missing login token."
    val INVALID_USERNAME_PASSWORD = "Invalid username or password."
    val INVALID_PASSWORD = "Incorrect Password."
    val INVALID_OTP = "Invalid OTP."
    val VALIDATION_FAILED = "Validation failed."
    val NO_USER_IN_SESSION = "No user in session"
    val ALREADY_SIGNED_UP = "You have already signed up with this account information."
    val INVALID_CHARS = "#!@$%^&*()+{}[]|<>,.~`/:;?-='£• ¢\"\\¥\\"
    val INVALID_EMPLOYER = "Invalid employer id."
    val USER_NOT_FOUND = "User not found: "
    val USER_INACTIVE = "User is inactive: "
    val TERMINAL_NOT_FOUND = "Your terminal ID is not in service."
    val INSUFFICIENT_BALANCE_ERROR_MASSAGE = "Requested Amount is greater than the Available Balance. "
    val NOT_AVAILABLE = "Not Available"
    val MOBILE_IN_USE = "Mobile Number already in use."
    val EMAIL_IN_USE = "Email already in use."
    val INVALID_USER = "Invalid user."
    val INCORRECT_PASSWORD = "Incorrect Password."
    val STEP_1 = 0
    val STEP_2 = 1
    val STEP_3 = 2
    val STEP_4 = 3
    val PASSWORD_MIN_LENGTH = 8
    val MAX_OTP_VERIFICATION_COUNT = 3
    val RESEND_OTP_MAX_LIMIT = 5
    val isInDebugMode = true
    val CURRENT_CURRENCY = "$"
    val AUTH_POLICY_MOBILE_PASSWORD = "MOBILE_PASSWORD"
    val AUTH_PASSWORD_KEY = "AUTH_PASSWORD_KEY"
    val AUTH_UNLOCK_PIN_KEY = "AUTH_UNLOCK_PIN_KEY"

    val UNLOCK_PIN_LENGTH = 4
    val OTP_LENGTH = 4
    val PIN_LENGTH = 4
    val OTP_TRY_MAX_LIMIT = 3
    val PASSWORD_TRY_MAX_LIMIT = 3
    val SSN_AND_CARD_TRY_MAX_LIMIT = 3
    val CODE_TRY_MAX_LIMIT = 3

    val MONEY_EARNED_SERVICE = "MoneyEarned"
    val ME_CARD_SERVICE_CATEGORY = "MEPAYCARD"

    val CHANGE_PIN = 13
    val PUSH_MONEY = 14
    val RESET_PIN = 15
    val MANAGE_CARD = 16
    val ACTIVATE_CARD = 17
    val ALERT_PREFERENCES = 18
    val SETTLED = "SETTLED"
    val PENDING = "PENDING"
    val SKIP_QUESTION = "Skip question"
    val EXCEPTION_ERROR_TYPE = 1
    val DENIED_ERROR_TYPE = 2
    val TOO_MANY_ATTEMPTS_ERROR_TYPE = 3
    val TOO_MANY_INCORRECT_VERIFICATION_ATTEMPTS = "TOO_MANY_INCORRECT_VERIFICATION_ATTEMPTS"
    val USER_BLOCKED = "USER_BLOCKED"
}
