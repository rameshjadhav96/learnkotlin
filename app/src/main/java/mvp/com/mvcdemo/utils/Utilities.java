package mvp.com.mvcdemo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;

public class Utilities {

    public static boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected();
    }

    public static String getBase64String(String cmgString) {
        byte[] data = new byte[0];
        try {
            data = cmgString.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String removeLastChar(String string) {
        if (TextUtils.isEmpty(string)) {
            return string;
        }
        return string.substring(0, string.length() - 1);
    }

}
