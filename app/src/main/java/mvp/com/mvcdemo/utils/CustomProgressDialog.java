package mvp.com.mvcdemo.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.TextView;

import mvp.com.mvcdemo.R;


public class CustomProgressDialog
{
    private static ProgressDialog progressDialog = null;

    public static void showProgressDialog(Context mContext)
    {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progressdialog);
        if (progressDialog.getWindow() != null)
        {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        progressDialog.setCancelable(false);
    }

    public static void dismissProgressDialog()
    {
        if (progressDialog != null)
        {
            progressDialog.dismiss();
        }
    }

    public static void setProgressDialogText(String text)
    {
        TextView textView = progressDialog.findViewById(R.id.textView1);
        textView.setText(text);
    }

}
