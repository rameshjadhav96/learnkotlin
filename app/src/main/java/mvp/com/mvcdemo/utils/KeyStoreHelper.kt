package mvp.com.mvcdemo.utils


import android.content.Context

import com.yakivmospan.scytale.Crypto
import com.yakivmospan.scytale.Options
import com.yakivmospan.scytale.Store

import javax.crypto.SecretKey

class KeyStoreHelper private constructor(context: Context) {
    private var store: Store? = null
    private var crypto: Crypto? = null

    private val symmetricKey: SecretKey
        get() {
            if (!store!!.hasKey(AppConstants.MONEY_EARNED_SERVICE)) {
                store!!.generateSymmetricKey(AppConstants.MONEY_EARNED_SERVICE, null)
            }
            return store!!.getSymmetricKey(AppConstants.MONEY_EARNED_SERVICE, null)
        }

    init {
        store = Store(context)
        if (!store!!.hasKey(AppConstants.MONEY_EARNED_SERVICE)) {
            store!!.generateSymmetricKey(AppConstants.MONEY_EARNED_SERVICE, null)
        }
        crypto = Crypto(Options.TRANSFORMATION_SYMMETRIC)
    }

    fun encrypt(text: String): String {
        return crypto!!.encrypt(text, symmetricKey)
    }

    fun decrypt(encryptedData: String): String {
        try {
            return crypto!!.decrypt(encryptedData, symmetricKey)
        } catch (exc: Exception) {
            return ""
        }

    }

    companion object {
        private var helper: KeyStoreHelper? = null

        fun getInstance(context: Context): KeyStoreHelper {
            if (helper == null) {
                helper = KeyStoreHelper(context)
            }
            return helper!!
        }
    }

}
