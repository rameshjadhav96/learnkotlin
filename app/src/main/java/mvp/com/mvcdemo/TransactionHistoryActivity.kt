package mvp.com.mvcdemo

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import mvp.com.mvcdemo.adapter.TransactionListAdapter
import mvp.com.mvcdemo.viewmodel.TransactionViewModel

class TransactionHistoryActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var transactionListAdapter: TransactionListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_list)
        recyclerView = findViewById(R.id.recyclerView)
        var transitionViewModel: TransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        transitionViewModel.getTransactionList().observe(this, Observer { transitionViewModels ->
            transactionListAdapter = TransactionListAdapter(this@TransactionHistoryActivity, transitionViewModels!!)
            recyclerView!!.setLayoutManager(LinearLayoutManager(this@TransactionHistoryActivity))
            recyclerView!!.adapter = transactionListAdapter
        })
    }
}
