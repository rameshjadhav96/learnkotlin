package mvp.com.mvcdemo.model

import java.io.Serializable

class CommonResponse : Serializable {
    var success: Boolean = false
    var code: String? = null
    var message: String? = null
}
