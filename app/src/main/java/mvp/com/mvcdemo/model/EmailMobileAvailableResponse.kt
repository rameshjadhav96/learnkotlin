package mvp.com.mvcdemo.model


import java.io.Serializable

class EmailMobileAvailableResponse : Serializable {
    var status: CommonResponse? = null
    var mobileNumberAvailable: Boolean = false
    var emailIDAvailable: Boolean = false

}
