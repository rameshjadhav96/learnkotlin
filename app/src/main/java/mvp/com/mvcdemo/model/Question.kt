package mvp.com.mvcdemo.model

import java.io.Serializable
import java.util.ArrayList

class Question : Serializable {
    var question: String? = null
    var type: String? = null
    var answer: ArrayList<String>? = null
}
