package mvp.com.mvcdemo.model

import java.util.ArrayList

class LoginDetailsResponse {
    var status: CommonResponse? = null
    var loginToken: Int = 0
    var site: String? = null
    var authPolicy: AuthPolicy? = null
    var customerSupportNumber: String? = null
    var customerSupportEmail: String? = null
    var mobileNumber: String? = null
    var forceUpgrade: String? = null
    var forceUpgradeTo: String? = null
    var tncVersion: String? = null
    var isSSNPresent: Boolean = false
    var isDOBPresent: Boolean = false
    var tncBaseURL: String? = null

    inner class AuthPolicy {
        var method: ArrayList<LoginMethod>? = null
    }

    inner class LoginMethod {
        var type: String? = null
        var location: String? = null
    }

}
