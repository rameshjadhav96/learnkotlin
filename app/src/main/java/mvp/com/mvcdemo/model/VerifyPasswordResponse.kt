package mvp.com.mvcdemo.model

import java.io.Serializable

class VerifyPasswordResponse : Serializable {
    var status: CommonResponse? = null
    var verificationToken: String? = null
}