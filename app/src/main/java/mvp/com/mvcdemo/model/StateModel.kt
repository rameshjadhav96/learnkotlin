package mvp.com.mvcdemo.model

import java.io.Serializable
import java.util.ArrayList

class StateModel {
    var states: StatesList? = null
    var status: CommonResponse? = null

    inner class StatesList {
        var states: ArrayList<StatesData>? = null

        inner class StatesData(var name: String, var code: String) : Serializable
    }

}
