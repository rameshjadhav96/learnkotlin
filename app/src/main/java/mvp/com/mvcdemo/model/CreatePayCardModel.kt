package mvp.com.mvcdemo.model

import java.util.ArrayList

class CreatePayCardModel {
    var status: CommonResponse? = null
    var responseCode: String? = null
    var responseDescription: String? = null
    var accountNumber: String? = null
    var pan: String? = null
    var RRN: String? = null
    var idologyTxnIDNumber: String? = null
    var idologyQuestion: ArrayList<Question>? = null
}
