package mvp.com.mvcdemo.model

import java.io.Serializable

class MECycleResponse : Serializable {
    var status: CommonResponse? = null
    var available_balance: AvailableBalance? = null
    var cycle_hours: String? = null
    var paycheck_date: String? = null
    var lastProcessed: String? = null
    var mePayCardServiceState: String? = null
    var minLoadAmount: Double = 0.toDouble()
    var mePayCardFeeType: String? = null
    var mePayCardFee: Double = 0.toDouble()

    inner class AvailableBalance : Serializable {
        var amount: AmountDetails? = null
    }

    inner class AmountDetails : Serializable {
        var currencyCode: String? = null
        var value: Double = 0.toDouble()
    }
}
