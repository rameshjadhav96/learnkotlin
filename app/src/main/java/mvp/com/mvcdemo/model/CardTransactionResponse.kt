package mvp.com.mvcdemo.model

import java.io.Serializable
import java.util.ArrayList

class CardTransactionResponse : Serializable {
    var status: CommonResponse? = null
    var repositionDateComplement: String? = null
    var repositionDate: String? = null
    var repositionTime: String? = null
    var moreIndicator: String? = null
    var repositionTimestamp: String? = null
    var numberOfRecords: String? = null
    var payCardActivity: ArrayList<CardTransaction>? = null

}
