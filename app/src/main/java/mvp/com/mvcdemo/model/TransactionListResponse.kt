package mvp.com.mvcdemo.model

import java.util.ArrayList

class TransactionListResponse {
    var status: CommonResponse? = null
    var transHistory: TransactionHistory? = null

    inner class TransactionHistory {
        var item: ArrayList<Transaction>? = null
    }
}
