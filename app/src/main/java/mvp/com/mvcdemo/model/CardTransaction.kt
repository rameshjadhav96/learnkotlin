package mvp.com.mvcdemo.model

import java.io.Serializable

class CardTransaction : Serializable {
    var transactionDescription: String? = null
    var merchantName: String? = null
    var merchantCity: String? = null
    var transactionStatus: String? = null
    var transactionDetail: String? = null
    var debitCreditIndicator: String? = null
    var transactionDate: String? = null
    var transactionTime: String? = null
    var transactionAmount: String? = null
    var transactionType: String? = null
    var systemTimestamp: String? = null
    var utcOffset: String? = null
}
