package mvp.com.mvcdemo.model

class Transaction {
    var type: String? = null
    var transId: String? = null
    var transTime: Long = 0
    var disposition: String? = null
    var amount: AmountDetails? = null
    var fee: AmountDetails? = null
    var meFee: AmountDetails? = null


    inner class AmountDetails {
        var currencyCode: String? = null
        var value: Double = 0.toDouble()
    }
}
