package mvp.com.mvcdemo.model

class RegistrationStep3Model {
    var ssn: String? = null
    var streetAddress: String? = null
    var aptOrSuite: String? = null
    var city: String? = null
    var postalCode: String? = null
    var state: String? = null
    var stateName: String? = null
}