package mvp.com.mvcdemo.model

import com.google.gson.annotations.SerializedName

import java.io.Serializable
import java.util.ArrayList

class CardDetailsResponse : Serializable {
    var status: CommonResponse? = null
    @SerializedName("moneyearned_balance")
    var available_balance: AvailableBalance? = null
    @SerializedName("payrollCardAccount")
    var payCardAccount: ArrayList<PayCardDetails>? = null

    inner class AvailableBalance : Serializable {
        var amount: AmountDetails? = null
    }

    inner class AmountDetails : Serializable {
        var currencyCode: String? = null
        var value: Double = 0.toDouble()
    }

    inner class PayCardDetails : Serializable {
        var cardLastFour: String? = null
        var expirationDate: String? = null // "06/21"
        var cardStatusCode: String? = null
        var accountNumber: String? = null
        var routingNumber: String? = null
        @SerializedName("id")
        var cardId: String? = null
        var balance: BalanceDetails? = null
        var balanceAlerts: Boolean = false
        var transactionAlerts: Boolean = false
        var highRiskTransactionAlerts: Boolean = false
    }

    inner class BalanceDetails : Serializable {
        var currencyCode: String? = null
        var value: Double = 0.toDouble()
    }
}
