package mvp.com.mvcdemo.model

class SessionResponse {
    val status: CommonResponse? = null
    var employeeID: String? = null
    var session_id: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var address1: String? = null
    var address2: String? = null
    var city: String? = null
    var state: String? = null
    var zip: String? = null
    var mobileNumber: String? = null
    var emailContact: String? = null
    var dob: String? = null
    var employeeState: String? = null
    var forceUpgrade: String? = null
    var forceUpgradeTo: String? = null
    var site: String? = null
    var siteName: String? = null
    var tncVersion: String? = null
    var customerSupportNumber: String? = null
    var customerSupportEmail: String? = null
    var tncBaseURL: String? = null
    var moneyEarnedAvailable: Boolean = false
    var payCardCustomerNumber: String? = null
}
