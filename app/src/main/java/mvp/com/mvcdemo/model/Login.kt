package mvp.com.mvcdemo.model

import android.databinding.BaseObservable
import android.text.TextUtils
import mvp.com.mvcdemo.utils.Utilities

/**
 * Created by ganart on 10/10/18.
 */
class Login(private var userName: String, private var password: String) : BaseObservable() {
    val isDataValid: Boolean
        get() = (!TextUtils.isEmpty(getUserName()) && !TextUtils.isEmpty(getPassword()) && getPassword().length >= 8)

    fun getUserName(): String {
        return userName
    }

    fun getPassword(): String {
        return password
    }

    fun setUserName(userName: String) {
        this.userName = userName
    }

    fun setPassword(password: String) {
        this.password = password
    }
}