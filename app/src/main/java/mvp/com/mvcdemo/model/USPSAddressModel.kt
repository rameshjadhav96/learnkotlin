package mvp.com.mvcdemo.model

import java.io.Serializable

class USPSAddressModel : Serializable {
    var status: CommonResponse? = null
    var address1: String? = null
    var address2: String? = null
    var city: String? = null
    var state: String? = null
    var stateName: String? = null
    var zip: String? = null
}
