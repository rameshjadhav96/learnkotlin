package mvp.com.mvcdemo.model

class MoneyEarnedModel {
    var status: CommonResponse? = null
    var RRN: String? = null
    var sessionID: String? = null
    var transaction: TransactionDetails? = null

    inner class TransactionDetails {
        var type: String? = null
        var requestedAmount: RequestedAmount? = null
    }

    inner class RequestedAmount {
        var currencyCode: String? = null
        var value: String? = null
    }

}
