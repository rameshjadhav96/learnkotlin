package mvp.com.mvcdemo.viewmodel

import android.arch.lifecycle.ViewModel
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import mvp.com.mvcdemo.interfaces.LoginCallbacks
import mvp.com.mvcdemo.model.Login
import mvp.com.mvcdemo.model.SessionResponse
import mvp.com.mvcdemo.network.ServiceClient
import mvp.com.mvcdemo.network.WebServiceBuilder
import mvp.com.mvcdemo.utils.CustomProgressDialog
import mvp.com.mvcdemo.utils.KeyStoreHelper
import mvp.com.mvcdemo.utils.SharedPreferenceManager
import mvp.com.mvcdemo.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginViewModel(private val context: Context, private val listener: LoginCallbacks, private val loginBtn: View) : ViewModel() {
    private val login: Login

    init {
        this.login = Login("", "")
    }

    val emailTextWatcher: TextWatcher
        get() = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                login.setUserName(s.toString())
                enableDisableButton(login.isDataValid)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        }
    val passwordTextWatcher: TextWatcher
        get() = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                login.setPassword(s.toString())
                enableDisableButton(login.isDataValid)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        }
    val textWatcher: TextWatcher
        get() = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                login.setPassword(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        }

    fun enableDisableButton(enable: Boolean) {
        loginBtn.isEnabled = enable
        loginBtn.alpha = if (enable) 1f else 0.5f

    }

    fun onLoginClicked(view: View) {
        if (login.isDataValid) {
            login()
        }
    }

    fun login() {
        CustomProgressDialog.showProgressDialog(context)
        val mPostAPIs = ServiceClient.getInstance().createService<WebServiceBuilder.PostAPIs>(WebServiceBuilder.PostAPIs::class.java)
        val call = mPostAPIs.login(login.getUserName(), Utilities.getBase64String(login.getPassword()), "true")
        call.enqueue(object : Callback<SessionResponse> {
            override fun onResponse(call: Call<SessionResponse>, response: Response<SessionResponse>) {
                if (response.isSuccessful) {
                    if (response.body().status != null) {
                        val sessionResponse1 = response.body();
                        if (sessionResponse1.status!!.success) {
                            val enteredPassword = login.getPassword()!!
                            val encryptedUserPassword = KeyStoreHelper.getInstance(context).encrypt(
                                    Utilities.getBase64String(enteredPassword))
                            SharedPreferenceManager.setEncryptedUserPassword(context, encryptedUserPassword)
                        }
                    }
                    listener.onSuccess(response.body())
                }
            }

            override fun onFailure(call: Call<SessionResponse>, t: Throwable) {
                listener.onFailure(t)
            }
        })
    }
}