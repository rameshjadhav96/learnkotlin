package mvp.com.mvcdemo.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import mvp.com.mvcdemo.model.Category

class CategoryViewModel : ViewModel {
    var id: String = ""
    var name: String = ""
    var description: String = ""

    constructor() : super()
    constructor(category: Category) : super() {
        this.id = category.id
        this.name = category.name
        this.description = category.description
    }

    var arrayListMutable = MutableLiveData<ArrayList<CategoryViewModel>>()
    var arrayList = ArrayList<CategoryViewModel>()

    fun getCategoryList(): MutableLiveData<ArrayList<CategoryViewModel>> {

        val category1 = Category("1", "First", "This is First ")
        val category2 = Category("2", "Second", "This is Second ")

        val categoryViewModel1 = CategoryViewModel(category1)
        val categoryViewModel2 = CategoryViewModel(category2)

        arrayList.add(categoryViewModel1)
        arrayList.add(categoryViewModel2)
        arrayListMutable.value = arrayList
        return arrayListMutable;
    }
}