package mvp.com.mvcdemo.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import mvp.com.mvcdemo.interfaces.TransactionCallbacks

class TransactionViewModelFactory(private val context: Context, private val listener: TransactionCallbacks) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TransactionViewModel(context, listener) as T
    }
}