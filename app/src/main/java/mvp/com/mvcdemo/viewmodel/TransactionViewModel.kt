package mvp.com.mvcdemo.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import mvp.com.mvcdemo.interfaces.TransactionCallbacks
import mvp.com.mvcdemo.model.Transaction
import mvp.com.mvcdemo.model.TransactionListResponse
import mvp.com.mvcdemo.network.ServiceClient
import mvp.com.mvcdemo.network.WebServiceBuilder
import mvp.com.mvcdemo.utils.CustomProgressDialog
import mvp.com.mvcdemo.utils.SharedPreferenceManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransactionViewModel(private val context: Context, private val listener: TransactionCallbacks) : ViewModel() {
    var type: String? = null
    var transId: String? = null
    var transTime: Long = 0
    var disposition: String? = null
    var amount: Transaction.AmountDetails? = null
    var fee: Transaction.AmountDetails? = null
    var meFee: Transaction.AmountDetails? = null

    var transactionMutableList = MutableLiveData<ArrayList<Transaction>>()
    var arrayList = ArrayList<Transaction>()

    fun getTransactionList(): MutableLiveData<ArrayList<Transaction>> {
        CustomProgressDialog.showProgressDialog(context)
        val mPostAPIs = ServiceClient.getInstance().createService<WebServiceBuilder.GetAPIs>(WebServiceBuilder.GetAPIs::class.java)
        val call = mPostAPIs.getTransactionListData(SharedPreferenceManager.getSessionID(context), SharedPreferenceManager.getEmployeeID(context))
        call.enqueue(object : Callback<TransactionListResponse> {
            override fun onResponse(call: Call<TransactionListResponse>, response: Response<TransactionListResponse>) {
                CustomProgressDialog.dismissProgressDialog()
                if (response.isSuccessful) {
                    if (response.body().status != null) {
                        val sessionResponse1 = response.body();
                        if (sessionResponse1.status!!.success) {
                            arrayList.addAll(sessionResponse1.transHistory!!.item!!)
                            transactionMutableList.value = arrayList
                        }
                    }
                    listener.onSuccess(response.body())
                }
            }

            override fun onFailure(call: Call<TransactionListResponse>, t: Throwable) {
                CustomProgressDialog.dismissProgressDialog()
            }
        })
        return transactionMutableList
    }

}