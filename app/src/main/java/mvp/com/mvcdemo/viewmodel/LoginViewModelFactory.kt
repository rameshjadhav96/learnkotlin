package mvp.com.mvcdemo.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.widget.Button
import mvp.com.mvcdemo.interfaces.LoginCallbacks

class LoginViewModelFactory(private val context: Context, private val listener: LoginCallbacks, private val loginBtn: Button) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(context, listener, loginBtn) as T
    }
}