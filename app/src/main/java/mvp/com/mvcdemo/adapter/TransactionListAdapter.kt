package mvp.com.mvcdemo.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import mvp.com.mvcdemo.R
import mvp.com.mvcdemo.databinding.TransactionBinding
import mvp.com.mvcdemo.model.Transaction
import mvp.com.mvcdemo.viewmodel.TransactionViewModel

public class TransactionListAdapter(private val context: Context, private val arrayList: ArrayList<Transaction>) : RecyclerView.Adapter<TransactionListAdapter.CustomView>() {
    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {
        val transactionViewModel = arrayList[position]
        holder.bind(transactionViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        val layoutInflater = LayoutInflater.from(context)
        val categoryBinding: TransactionBinding = DataBindingUtil.inflate(layoutInflater, R.layout.transaction_row, parent, false)
        return CustomView(categoryBinding)
    }

    class CustomView(val transactionBinding: Transaction) : RecyclerView.ViewHolder(transactionBinding.root) {
        fun bind(transactionViewModel: Transaction) {
            this.transactionBinding.transactionModel = transactionViewModel
            transactionBinding.executePendingBindings()
        }
    }
}