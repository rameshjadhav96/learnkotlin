package mvp.com.mvcdemo.interfaces

import mvp.com.mvcdemo.model.SessionResponse

interface LoginInterface {

    interface Presenter {
        fun onDestroy()

        fun onLoginButtonClick(username: String, enteredPassword: String)
    }

    interface LoginView {
        fun showProgress()

        fun hideProgress()

        fun onLoginSuccess(sessionResponse: SessionResponse)

        fun loginFailure(throwable: Throwable)
    }

    interface LoginIntractor {

        interface OnFinishedListener {
            fun onFinished(body: SessionResponse)

            fun onFailure(t: Throwable)
        }

        fun requestLogin(username: String, enteredPassword: String, finishedListener: OnFinishedListener)
    }
}
