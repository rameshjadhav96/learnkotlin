package mvp.com.mvcdemo.interfaces

import mvp.com.mvcdemo.model.TransactionListResponse

interface TransactionCallbacks {
    fun onSuccess(transactionListResponse: TransactionListResponse)
    fun onFailure(trowable: Throwable)
}