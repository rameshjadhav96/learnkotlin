package mvp.com.mvcdemo.interfaces

import mvp.com.mvcdemo.model.SessionResponse

interface LoginCallbacks {
    fun onSuccess(sessionResponse: SessionResponse)
    fun onFailure(trowable: Throwable)
}