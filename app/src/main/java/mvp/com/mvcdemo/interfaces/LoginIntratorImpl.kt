package mvp.com.mvcdemo.interfaces


import mvp.com.mvcdemo.model.SessionResponse
import mvp.com.mvcdemo.network.ServiceClient
import mvp.com.mvcdemo.network.WebServiceBuilder
import mvp.com.mvcdemo.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginIntratorImpl : LoginInterface.LoginIntractor {
    override fun requestLogin(username: String, enteredPassword: String, finishedListener: LoginInterface.LoginIntractor.OnFinishedListener) {
        val mPostAPIs = ServiceClient.getInstance().createService<WebServiceBuilder.PostAPIs>(WebServiceBuilder.PostAPIs::class.java)
        val call = mPostAPIs.login(username, Utilities.getBase64String(enteredPassword), "true")
        call.enqueue(object : Callback<SessionResponse> {
            override fun onResponse(call: Call<SessionResponse>, response: Response<SessionResponse>) {
                if (response.isSuccessful) {
                    finishedListener.onFinished(response.body())
                }
            }

            override fun onFailure(call: Call<SessionResponse>, t: Throwable) {
                finishedListener.onFailure(t)
            }
        })
    }
}
