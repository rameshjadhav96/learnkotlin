package mvp.com.mvcdemo

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import mvp.com.mvcdemo.databinding.ActivityLoginBinding
import mvp.com.mvcdemo.interfaces.LoginCallbacks
import mvp.com.mvcdemo.model.SessionResponse
import mvp.com.mvcdemo.utils.CustomProgressDialog
import mvp.com.mvcdemo.utils.SharedPreferenceManager
import mvp.com.mvcdemo.viewmodel.LoginViewModel
import mvp.com.mvcdemo.viewmodel.LoginViewModelFactory

class LoginActivity : AppCompatActivity(), LoginCallbacks {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val loginActivityBinding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        val loginBtn = loginActivityBinding.loginBtn
        loginActivityBinding.loginModel = ViewModelProviders.of(this, LoginViewModelFactory(this, this, loginBtn)).get(LoginViewModel::class.java)
    }

    override fun onSuccess(sessionResponse: SessionResponse) {
        CustomProgressDialog.dismissProgressDialog()
        if (sessionResponse.status != null) {
            if (sessionResponse.status.success) {
//                val enteredPassword = passwordEdit!!.getText().toString().trim()
//                val encryptedUserPassword = KeyStoreHelper.getInstance(applicationContext).encrypt(
//                        Utilities.getBase64String(enteredPassword))
//                SharedPreferenceManager.setEncryptedUserPassword(this, encryptedUserPassword)
                SharedPreferenceManager.setSessionResponse(this, sessionResponse)
                SharedPreferenceManager.setUserSignedUp(this, true)
                startActivity(Intent(this, TransactionHistoryActivity::class.java))
                Toast.makeText(this, "Login Success", Toast.LENGTH_LONG).show()
            } else if (!TextUtils.isEmpty(sessionResponse.status.message)) {
                if (sessionResponse.status.message!!.startsWith("Incorrect Password.") || sessionResponse.status.message!!.startsWith("Invalid username or password.")) {
                    Toast.makeText(this, getString(R.string.invalid_username_passwd), Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, sessionResponse.status.message, Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.unknown_error_occurred), Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(this, getString(R.string.unknown_error_occurred), Toast.LENGTH_LONG).show()
        }
    }

    override fun onFailure(throwable: Throwable) {
        CustomProgressDialog.dismissProgressDialog()
        Toast.makeText(this@LoginActivity,
                "Something went wrong...Error message: " + throwable.message,
                Toast.LENGTH_LONG).show()
    }

}
