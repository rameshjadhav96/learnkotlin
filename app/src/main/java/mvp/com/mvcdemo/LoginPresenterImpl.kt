package mvp.com.mvcdemo

import mvp.com.mvcdemo.interfaces.LoginInterface
import mvp.com.mvcdemo.model.SessionResponse

class LoginPresenterImpl(private var loginView: LoginInterface.LoginView?, private val loginIntractor: LoginInterface.LoginIntractor) : LoginInterface.Presenter, LoginInterface.LoginIntractor.OnFinishedListener {

    override fun onDestroy() {
        loginView = null
    }

    override fun onLoginButtonClick(username: String, enteredPassword: String) {
        if (loginView != null) {
            loginView!!.showProgress()
        }
        loginIntractor.requestLogin(username, enteredPassword, this)
    }

    override fun onFinished(body: SessionResponse) {
        if (loginView != null) {
            loginView!!.hideProgress()
            loginView!!.onLoginSuccess(body)
        }
    }

    override fun onFailure(t: Throwable) {
        if (loginView != null) {
            loginView!!.hideProgress()
            loginView!!.loginFailure(t)
        }
    }
}
